import 'package:eznior/model/jsonmodel.dart';
import 'package:eznior/model/viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import '../widgets/widget.dart';
import '../constants.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  final TextEditingController _nama = new TextEditingController();
  final TextEditingController _harga = new TextEditingController();
  final TextEditingController _gambar = new TextEditingController();

  List dataUser = new List();
  int _value = 1;

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: kBackgroundColor,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Image(
              width: 24,
              color: Colors.white,
              image: Svg('assets/images/back_arrow.svg'),
            ),
          ),
        ),
        backgroundColor: kBackgroundColor,
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    children: [
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Tambah Barang",
                              style: kHeadline,
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            MyTextField(
                              control: _nama,
                              hintText: 'Nama ',
                              inputType: TextInputType.name,
                            ),
                            MyTextField(
                              control: _harga,
                              hintText: 'Harga',
                              inputType: TextInputType.name,
                            ),
                            MyTextField(
                              control: _gambar,
                              hintText: 'Url Gambar',
                              inputType: TextInputType.name,
                            ),
                            Container(
                              color: Colors.white,
                              padding: EdgeInsets.all(5.0),
                              child: DropdownButton(
                                hint: Text(
                                  "Pilih Jenis",
                                  style: TextStyle(color: Colors.white),
                                ),
                                value: _value,
                                items: [
                                  DropdownMenuItem(
                                    child: Text("Modern"),
                                    value: 1,
                                  ),
                                  DropdownMenuItem(
                                    child: Text("Tradisional"),
                                    value: 2,
                                  ),
                                  DropdownMenuItem(
                                    child: Text("Aksesors"),
                                    value: 3,
                                  )
                                ],
                                onChanged: (int value) {
                                  setState(() {
                                    _value = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyTextButton(
                        buttonName: 'Tambah',
                        onTap: () {
                          UserpostModel commRequest = UserpostModel();
                          commRequest.nama = _nama.text;
                          commRequest.harga = _harga.text;
                          commRequest.url_gambar = _gambar.text;
                          commRequest.jenis = _value.toString();

                          UserViewModel()
                              .postUser(userpostModelToJson(commRequest))
                              .then((value) => print('success'));

                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Pemberitahuan'),
                                  content: Container(
                                    child: Text('Oke menambahkan' +
                                        _nama.text +
                                        " " +
                                        _gambar.text +
                                        " " +
                                        _harga.text +
                                        " " +
                                        " " +
                                        _value.toString()),
                                  ),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text('close'))
                                  ],
                                );
                              });
                        },
                        bgColor: Colors.white,
                        textColor: Colors.black87,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
