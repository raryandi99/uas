import 'package:flutter/material.dart';
import 'package:eznior/model/get_model.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  UserGet userGet;

  @override
  void initState() {
    super.initState();
    UserGet.connectToApiUser('4').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(body: _bodyProfile()));
  }

  Widget _bodyProfile() {
    return Center(
        child: Container(
      height: MediaQuery.of(context).size.height,
      color: Colors.blue[100],
      alignment: Alignment.center,
      padding: EdgeInsets.all(30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            "WELCOME!",
            style: TextStyle(fontSize: 48, color: Color(0xff0563BA)),
          ),
          Container(
              child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(70.0),
                child: Image.network(
                  userGet.avatar,
                  cacheHeight: 128,
                  cacheWidth: 128,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  (userGet != null)
                      ? userGet.fristName + " " + userGet.lastName
                      : "Data Gagal Diambil",
                  style: TextStyle(fontSize: 24, color: Color(0xff0563BA)),
                ),
              ),
              Text(
                (userGet != null) ? userGet.email : "Data Gagal Diambil",
                style: TextStyle(fontSize: 18, color: Colors.black),
              )
            ],
          ))
        ],
      ),
    ));
  }
}
