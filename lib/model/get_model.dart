import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id;
  String fristName;
  String lastName;
  String email;
  String avatar;

  UserGet({this.id, this.fristName, this.lastName, this.email, this.avatar});

  factory UserGet.createUserGet(Map<String, dynamic> object) {
    //return objectpostResult yang baru
    return UserGet(
      id: object['id'].toString(),
      fristName: object['first_name'],
      lastName: object['last_name'],
      email: object['email'],
      avatar: object['avatar'],
    );
  }

  static Future<UserGet> connectToApiUser(String id) async {
    String apiURLPOST = 'sttps://reqres.in/api/users/' + id;

    var apiResult = await http.get(apiURLPOST);

    var jsonObject = jsonDecode(apiResult.body);

    var userData = (jsonObject as Map<String, dynamic>)['data'];

    return UserGet.createUserGet(userData);
  }
}
