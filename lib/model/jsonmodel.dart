import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({this.nama, this.harga, this.url_gambar, this.jenis});

  String nama;
  String harga;
  String url_gambar;
  String jenis;

  Map toJson() =>
      {"nama": nama, "harga": harga, "gambar": url_gambar, "jenis": jenis};
}
