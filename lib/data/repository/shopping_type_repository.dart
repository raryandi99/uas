import 'package:eznior/data/entity/shopping_type.dart';

class FakeShoppingTypeGenerator {
  List<ShoppingType> types = [
    ShoppingType("Modern",
        "https://s2.bukalapak.com/bukalapak-kontenz-production/content_attachments/60587/w-744/alat_musik_modern_main.jpg"),
    ShoppingType("Tradisional",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_zq0tlB9jeFuIQDlpBclnyd6qsZ-MNsFLJw&usqp=CAU"),
    ShoppingType("Aksesoriss",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTWsnDR3v_5eRLWCVjjWfsB2dE9NZ4FlIVLjg&usqp=CAU"),
  ];

  List<ShoppingType> generate() {
    return types;
  }
}
